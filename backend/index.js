const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt  = require ('jsonwebtoken')
const dotenv = require('dotenv')
const TOKEN_SECRET = process.env.TOKEN_SECRET


const mysql = require('mysql');

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'admin',
    password :'5555',
    database : 'DormbookingSystem'

})

connection.connect();

const express = require('express')
const app = express()
const port = 4000


/*Middlewere for Autheticatiny User Token */


function AutheticateTiken(req,res, next){
    const authMeader = req.headers['authorization']
    const token = authMeader && authMeader.split('')(1)
   if (token == null ) return res.sendStatus(401)

   jwt.verify(token,TOKEN_SECRET, (err, user) 



app.get("/list_booking",( req,res ) => {
    query = "SELECT * from Booking";
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    });
})

app.post("/add_room", (req, res) => {

    let room_price = req.query.room_price
    


    let query = `INSERT INTO Room (RoomPrice) VALUES ('${room_price}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Adding Booking succesful"
            })
        }
    })
})
app.post("/update_room", (req, res) => {


    let room_id   = req.query.room_id
    let room_price    = req.query.room_price
    

    let query = `UPDATE room SET
    RoomPrice = '${room_price}' WHERE  RoomID = '${room_id}'` 

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Updating succesful"
            })
        }
    })
})

app.post("/delete_room", (req, res) => {

    let room_id    = req.query.room_id

    let query = `DELETE FROM Room
                 WHERE RoomID = '${room_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Deleting  Room succesful"
            })
        }
    })
})
  
app.post("/login", (req, res) => {

    let username = req.query.username
    let password = req.query.password
    let query = `SELECT * FROM Buffeter WHERE Username= '${username}'`
    connection.query(query, (err, rows) =>{
        if (err){
            console.log(err)
            res.json({"status" : "400",
                         "message" : "Error querying from running db"  
                        })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "userid"  : rows[0].UserD,
                        "IsAdmin"  : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.send(token)
                }else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})



/*APO for Registering to a new Running Event */
app.post("/register_user", (req, res) => {
    
    let user_name = req.query.userr_name
    let user_surname = req.query.user_surname
    let user_pass = req.query.user_pass
    let user_email = req.query.userr_email

    bcrypt.hash(user_pass, SALT_ROUNDS, (err, hash) =>{
        let query = `INSERT INTO User (UserName, UserSurname, Email, Password, IsAdmin )
                VALUES ('${user_name}', '${user_surname}', '${user_email}','${hash}', false)`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Registering succesful"
                })
             }
        })
    })
})








app.listen(port, () => {
    console.log(`Now starting Dormbooking System Backend  ${port}`)
})






/*query = "SELECT * from Booking";
connection.query( query,(err,rows) => {
            if (err){
                console.log(err)
            }else{
                console.log(rows)
            }
})
connection.end(); */